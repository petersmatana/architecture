import msgpack
import json

from kafka import KafkaProducer
from kafka.errors import KafkaError

producer = KafkaProducer(bootstrap_servers=['0.0.0.0:29092'])

future = producer.send('my-topic', b'asd')
print(f'send message: {future}')

producer.send('my-topic', key=b'foo', value=b'bar')

# encode objects via msgpack
producer = KafkaProducer(
    bootstrap_servers=['0.0.0.0:29092'],
    value_serializer=msgpack.dumps)
producer.send('my-topic', {'key': 'value'})

# produce json messages
producer = KafkaProducer(
    bootstrap_servers=['0.0.0.0:29092'],
    value_serializer=lambda m: json.dumps(m).encode('ascii'))
producer.send('my-topic', {'key2': 'value'})

with open('slovicka', encoding='utf8') as f:
    for line in f:
        future = producer.send('my-topic', line.replace('\n', ''))


producer.flush()

# # produce keyed messages to enable hashed partitioning
# producer.send('my-topic', key=b'foo', value=b'bar')

# # encode objects via msgpack
# # producer = KafkaProducer(value_serializer=msgpack.dumps)
# # producer.send('msgpack-topic', {'key': 'value'})

# # produce json messages
# producer = KafkaProducer(value_serializer=lambda m: json.dumps(m).encode('ascii'))
# producer.send('json-topic', {'key': 'value'})

# # produce asynchronously
# for _ in range(100):
#     producer.send('my-topic', b'msg')

# def on_send_success(record_metadata):
#     print(record_metadata.topic)
#     print(record_metadata.partition)
#     print(record_metadata.offset)

# def on_send_error(excp):
#     log.error('I am an errback', exc_info=excp)
#     # handle exception

# # produce asynchronously with callbacks
# producer.send('my-topic', b'raw_bytes').add_callback(on_send_success).add_errback(on_send_error)

# # block until all async messages are sent
# producer.flush()

# # configure multiple retries
# producer = KafkaProducer(retries=5)
