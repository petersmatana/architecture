# 1. run docker containers

First need to run this docker compose.

`docker-compose -f kafka.yml up -d`

# 2. python packages

Need to have this python packages:

Package        Version
-------------- -------
kafka-python   2.0.2
msgpack-python 0.5.6
pip            22.0.2
setuptools     59.6.0

# 3. run consumer

Then need to run consumer because when producent is run, messages should be consumed.

`python consumer.py`

# 4. run producent

Producent produced some data which consumer consumed.

`python producent.py`
