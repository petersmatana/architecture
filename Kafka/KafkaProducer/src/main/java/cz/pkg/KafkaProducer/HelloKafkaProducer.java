package cz.pkg.KafkaProducer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.

import org.springframework.kafka.core.KafkaTemplate;
// import org.springframework.kafka.core.KafkaProducerConfig;

@Configuration
public class HelloKafkaProducer {

    @Autowired
    private KafkaTemplate<String, String> template;

    public HelloKafkaProducer() {
    }

    @Bean
    public void startMethod() {
        // this.template = new KafkaTemplate<>(new KafkaProducerConfig().kafkaTemplate());
        this.template.send("hello", "Hello, Kafka!");
    }

}
