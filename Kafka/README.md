# Kafka concepts

**Publisher / Producer** is someone who sends messages to Kafka.

**Consumer / Subscriber** is someone who reads messages from Kafka. Consumer keep track of the offset of the messages they read. Consumer can rewind or skip to any point in a partition simply by supplying an offset value.

**Kafka Broker** is a server that stores messages. There are one or more servers.

**Cluster** is a group of Kafka Brokers. Typically managed by Zookeeper. KRaft is Kafka Cluster without Zookeeper.

**Topic** is where messages are stored. If a producer want to send a message, is send to a topic. If a consumer want to read a message, is read from a topic. Analogue can be table in database world and folder in file system. Topic is append only.

**Partition** is division of a topic. Partition allow parallelize a topic by splitting in multiple partitions. Each partition can be placed on separate broker. *I am not sure right now If I can point in which partition message will be stored. I think it is random.*

**Offset** - If there are multiple partitions, using round robin, each message is stored in a partition. Offset is the position (to be exact it is integer that continually increases.) of the message in the partition. Offset is unique for each partition. Zero is the oldest message is the partition, and the highest offset is the newest message in the partition.
