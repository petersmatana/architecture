# learn k8n

## Master Node

Contains `Control Plane` which consists of:

### API Server

- Is proxy to all of the components on Master node. Or frontend to Kubernetes Control Plane.
- All communication - external and internal goes through API Server.
- Exposes RESTful API on port 443.
- Authentication and Authorization checks. 

### Scheduler

- Watch for new workload/pods and assign them node (which is based on several scheduling factors - healthy, enough resources, port available).

### Cluster Store

- Stores configuration and state of entire cluster.
- Kubernetes using etcd which is distributed key-value data store.

### Control Manager

- Daemon that check:
  - ReplicaSet
  - Endpoint
  - Namespaces
  - Service Accounts
- Watching for changes and provide them to desired state.

### CLoud Control Manager

- Connect to 3rd party cloud provider like AWS, Azure, GCP...

## Work node

Running env for workload. 3 main component:

### Kublet

### Container Runtime

### Kube Proxy

# how install minikube

## 1. download

link: https://minikube.sigs.k8s.io/docs/start/

install can be done without root: `curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
`

## 2. start

`minikube start`

## 3. sample app

`minikube kubectl -- create deployment firstapp --image=amigoscode/kubernetes:hello-world --port=80`

which returns:

*deployment.apps/firstapp created*

and show how deployment happened - run command: `minikube kubectl -- get pods -A`

I can make port-forwarding by: `minikube kubectl port-forward <pod_name> 8080:80` but somehow this is not change for ever. this command needs to be terminated in CLI.

Delete can be done by: `minikube kubectl delete <pod_name>` but pod is restored and appear with new unique name... This is probably done by replica-set.

Show all pods: `minikube kubectl -- get pods -A`


