import React from "react";

export default function StateComponent(props) {

    /*
    * useState returns an array with two elements:
    * 1. the current state
    * 2. a function to update the state.
    *   This function is called a setter function.
    *   Conventionally, the setter function is named set + 
    *       the name of the state variable.
    */
    const [counter, setCounter] = React.useState(props.initValue);

    function add() {
        /*
        * The setter function is used to update the state.
        * The new state is passed as an argument to the setter function.
        * Cant use counter += 1, because it would not trigger a re-render.
        */
        setCounter(counter + 1)
    }

    /*
    * Subtract function will be implemented as call back function.
    * This is best practice but right understanding need deeper knowledge
    * of handling state and rendering components.
    * 
    * oldValue points to counter variable, which is the current state.
    */
    function subtract() {
        setCounter(function(oldValue) {
            return oldValue - 1
        })
    }

    // implemented as arrow function
    function addFive() {
        setCounter(oldValue => oldValue + 5)
    }

    return (
        <div>
            <p>result: {counter}</p>
            <button onClick={add}>+</button>
            <button onClick={subtract}>-</button>
            <button onClick={addFive}>+5</button>
        </div>
    );
}