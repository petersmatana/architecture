import React from "react";

export default function PassingObject(props) {
    return (
        <div>
            key: {props.myObject.key1}<br />
            key2: {props.myObject.key2}<br />
            key3: {props.myObject.key3.toString()}<br />
            key4: {props.myObject.key4} - key4 never exist
        </div>
    );
}