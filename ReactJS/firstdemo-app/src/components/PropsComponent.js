import React from "react";

export default function PropsComponent(props) {
  return (
    <p>props: {props.name} and some data: {props.data}</p>
  );
}