import './App.css';
import FirstComponent from "./components/FirstComponent";
import PropsComponent from "./components/PropsComponent";
import PassingObject from "./components/PassingObject";
import StateComponent from "./components/StateComponent";

const name = "world";
const element = <h1>Hello {name}</h1>;

const number = 10;
function isBiggerThanFive(compareNumber) {
  if (compareNumber > 5) {
    return <p>{compareNumber} is bigger than 5</p>;
  } else {
    return <p>{compareNumber} is smaller than 5</p>;
  }
}

function SmallComponent() {
  return <p>small component</p>;
}

function AcceptProps(props) {
  return <p>key: {props.myKey}, key2: {props.myKey2}</p>;
}

const myObject = {
  key1: "value",
  key2: 123,
  key3: true
}

function App() {
 
  let clickCounter = 0;
  const someArray = [1, 2];

  function handleClick() {
    console.log("clicked " + clickCounter + " times");
    clickCounter += 1;
  }

  return (
    <div className="App">

      {/* this is how CSS basically works */}
      <div className="My-Div">
        {/* evaluation of JSX */}
        {element}
      </div>
      {isBiggerThanFive(number)}

      {/* create component */}
      <SmallComponent />
      <FirstComponent />

      {/* pass props to component */}
      <PropsComponent name="123" data="asd" />

      <AcceptProps myKey="value" myKey2="value2" />
      <PassingObject myObject={myObject} />

      {someArray.map((item) => {
        return <p key={item}>{item.toString()}</p>;
      })}

      <button onClick={handleClick}>I was clicked {clickCounter} times</button>

      <StateComponent initValue={21} />
    </div>
  );
}

export default App;
