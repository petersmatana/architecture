# Hands dirty part

## Install part

Need to have node (now in version 16.18) and npm (now in 8.19).

## Create app

`npx create-react-app firstdemo-app`

This will probably fetch some dependencies.

## Run app

`npm start`

## JSX

JSX is a syntax extension to JavaScript. It is similar to a template language, but it has full power of JavaScript.

```
// This is JSX
const name = "world";
const element = <h1>Hello {name}</h1>;

// This is also JSX
const number = 10;
function isBiggerThanFive(compareNumber) {
  if (compareNumber > 5) {
    return <p>{compareNumber} is bigger than 5</p>;
  } else {
    return <p>{compareNumber} is smaller than 5</p>;
  }
}
```
## Props

React do not care If props are changed. It will do not re-render the component.

Props in my point of view is like call constructor in Java. It is immutable.

In parrent component:
```
// sending to props.name and props.data some data
<PropsComponent name="123" data="asd" />
```

Which is used in component:
```
export default function PropsComponent(props) {
  // no modification with props should be done here
  return (
    <p>props: {props.name} and some data: {props.data}</p>
  );
}
```

## State

Props are immutable. State is mutable. When props are set into component, they are immutable. When state is set into component, it is mutable.

```
export default function StateComponent(props) {
    // unwrap props. first: defacto data, second: function to change data
    const [counter, setCounter] = React.useState(props.initValue);

    // so increment number is:
    function increment() {
        // set counter is function to change data
        setCounter(counter + 1);
    }
}
```

# Hands not dirty part

## Hooks, Redux
Simple it is "function". It can for example change state. But it can be dangerous for bigger projects.

## Redux
Some change in one component can affect other component. Redux is a state container which manage state. It is like a global variable.

// to be continued:
https://youtu.be/bMknfKXIFA8?t=21998

other tutorial:
https://www.youtube.com/watch?v=u6gSSpfsoOQ
