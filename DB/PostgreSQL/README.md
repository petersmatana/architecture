# how to

go with all_together -> postgresql + pgadmin + adminer

hints:
- db, pgadmin and adminer need some time to boot, link do not response immediately
- If need to provide hostname of some service, it is: postgresql_db or adminer_ui or pgadmin_ui

## dvdrental - sample db

source: https://www.postgresqltutorial.com/postgresql-getting-started/postgresql-sample-database/

how to load data into db:
1. have in docker-compose.yml this: `- /home/smonty/:/home` where host dir /home/smonty is connected to container /home.
2. docker exec -ti <id> bash
3. cd /home/path/to/dvdrental.tar
4. pg_restore -U user -d dvdrental dvdrental.tar
