# Instalace
prikaz: `dnf install postgresql postgresql-server`

asi jedina zavislos je tento balik: libpq

# Setup
## Inicializace
pokud spoustim prikaz `systemctl status postgresql` tak mi to pravdepodobne vrati chybu ze DB je treba inicializovat

inicializace prikazem:
`postgresql-setup --initdb`

vystup:
```
 * Initializing database in '/var/lib/pgsql/data'
 * Initialized, logs are in /var/lib/pgsql/initdb_postgresql.log
```

## Vzdaleny pristup
### postgresql.conf
v souboru `/var/lib/pgsql/data/postgresql.conf` je potreba pridat: `listen_addresses = '*'` coz je poslouchani na vsech interface

### pg_hba.conf
V souboru `/var/lib/pgsql/data/pg_hba.conf` zakomentovat vsechny radky a pridat radek: `host all all 0.0.0.0/0 md5`

## Nove heslo uzivatele postgres
pokud se to povede je treba DB restartovat: `systemctl restart postgresql`

pokud se to povede a databaze jede, je vhodne nastavit uzivateli postgres nove heslo
1. prihlasit se jako root `su -`
2. prihlasit se jako postgres `su postgres`
3. spustit konzoli `psql`
4. spustit SQL: `alter user postgres with password 'password';`
5. konzole se ukoncuje pomoci `\q`

# Odinstalovani
`dnf remove postgresql-server`

`dnf remove postgresql`
